<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Form Siswa</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>

<form class="form-horizontal" method="POST" action="form_siswa.php">
<fieldset>

<!-- Form Name -->
<legend>Form Nilai Siswa</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nm_lengkap">Nama Lengkap</label>  
  <div class="col-md-5">
  <input id="nm_lengkap" name="nm_lengkap" type="text" placeholder="Masukan Nama" class="form-control input-md">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="matkul">Mata Kulian</label>
  <div class="col-md-5">
    <select id="matkul" name="matkul" class="form-control">
      <option value="1">Pemograman Web</option>
      <option value="2">Metode Penelitian</option>
      <option value="3">Basis Data II</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nuts">Nilai UTS</label>  
  <div class="col-md-4">
  <input id="nuts" name="nuts" type="text" placeholder="Nilai UTS" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nuas">Nilai UAS</label>  
  <div class="col-md-4">
  <input id="nuas" name="nuas" type="text" placeholder="Nilai UAS" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ntugas">Nilai TugasPraktikum</label>  
  <div class="col-md-4">
  <input id="ntugas" name="ntugas" type="text" placeholder="Nilai Tugas Praktikum" class="form-control input-md">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="simpan"></label>
  <div class="col-md-4">
    <button id="simpan" name="simpan" class="btn btn-primary">Simpan</button>
  </div>
</div>

</fieldset>
</form>

<?php
  
  $simpan = $_POST['simpan'];
  $nm_lengkap = $_POST['nm_lengkap'];
  $matkul = $_POST['matkul'];
  $nuts = $_POST['nuts'];
  $nuas = $_POST['nuas'];
  $ntugas = $_POST['ntugas'];

  //tampilkan

  echo'Proses : Tersimpan'.$simpan ;
  echo'<br/>Nama :'.$nm_lengkap ;
  echo'<br/>Mata Kuliah :'.$matkul ;
  echo'<br/>Nilai UTS :'.$nuts ;
  echo'<br/>Nilai UAS :'.$nuas;
  echo'<br/>Nilai Tugas Praktikum :'.$ntugas ;

?>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>




</body>
</html>
